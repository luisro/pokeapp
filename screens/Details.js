import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function Details(props) {
  const pokemonData = props.route.params;
  const [image, setImage] = useState('');
  const [height, setHeight] = useState(0);
  const [weight, setweight] = useState(0);
  const [abilities, setAbilities] = useState([]);
  const [types, setTypes] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    fetch(pokemonData.url)
      .then(res => res.json())
      .then(res => {
        setImage(res.sprites.other.home.front_default),
          setweight(res.weight),
          setHeight(res.height),
          setAbilities(res.abilities),
          setTypes(res.types),
          setLoading(false);
      })
      .catch(error => {
        console.log('Error ocurred:' + error.message);
      });
  }, []);

  return (
    <View style={styles.container}>
      {loading ? (
        <View style={styles.viewLoading}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      ) : (
        <View style={styles.viewInfo}>
          <View style={styles.viewHeader}>
            <View style={styles.viewImage}>
              <Text style={styles.titleHeader}>{pokemonData.name}</Text>
              <Text style={styles.mainInfo}>Weight: {weight}</Text>
              <Text style={styles.mainInfo}>Height: {height}</Text>
              <Image style={styles.img} source={image ? { uri: image } : null} />
            </View>
          </View>
          <View style={styles.viewContent}>
            <View style={styles.viewDetails}>
              <View style={styles.headerDetail}>
                <Text style={styles.titleInfo}>Habilities</Text>
              </View>
              {abilities.map(ability => (
                <Text style={styles.textDetails}>
                  {' '}
                  <Icon name="check" size={20} color="#0EA1CA" />{' '}
                  {ability.ability.name}
                </Text>
              ))}
            </View>
            <View style={styles.viewDetails}>
              <View style={styles.headerDetail}>
                <Text style={styles.titleInfo}>Types</Text>
              </View>
              {types.map(type => (
                <Text style={styles.textDetails}>
                  {' '}
                  <Icon name="check" size={20} color="#0EA1CA" />{' '}
                  {type.type.name}
                </Text>
              ))}
            </View>
          </View>
        </View>
      )}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7FB3D5',
  },
  viewInfo: {
    flex: 1,
  },
  viewLoading: {
    justifyContent: 'center',
    flex: 1,
  },
  titleHeader: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#2471A3',
  },
  textDetails: {
    paddingLeft: 30,
    paddingBottom:20,
    fontSize: 20,
    color: '#1B4F72',
  },
  titleInfo: {
    fontSize: 25,
    fontWeight: 'bold',
    color: '#2471A3',
  },
  viewHeader: {
    flex: 1,
    marginTop: 50,
    alignItems: 'center',
  },
  viewImage: {
    flex: 1,
    width: '90%',
    borderRadius: 250,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D7DBDD',
  },
  headerDetail: {
    alignItems: 'center',
    marginBottom: 30,
  },
  viewDetails: {
    flex: 1,
    marginTop: 20,
  },
  viewContent: {
    flex: 1,
    flexDirection: 'row',
    marginTop: 50,
    backgroundColor: '#D7DBDD',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  img: {
    width: 200,
    height: 200,
  },
  viewTitle: {
    flex: 1,
    alignItems: 'flex-end',
  },
  viewData: {
    flex: 2,
  },
  mainInfo: {
    fontSize: 12,
    color: 'black',
  },
});
export default Details;
