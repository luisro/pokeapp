import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

function Home({ navigation }) {
  const [isError, setError] = useState(false);
  const [pokemones, setPokemones] = useState([]);
  const [total, setTotal] = useState(100);
  useEffect(() => {
    getPokemones(0);
  }, []);
  const showMore = () => {
    let ofsset = total;
    getPokemones(ofsset);
    setTotal(total + 100);
  };
  const restore = () => {
    setTotal(100);
    getPokemones(0);
  }
  const getPokemones = value => {
    fetch(`https://pokeapi.co/api/v2/pokemon?limit=100&offset=${value}`)
      .then(res => res.json())
      .then(res => (value===0) ? setPokemones(res.results) : setPokemones([...pokemones, ...res.results]))
      .catch(() => setError(true));
  };
  const renderSeparator = () => {
    return <View style={styles.separator} />;
  };
  const renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity
        key={index}
        onPress={() =>
          navigation.navigate('Details', { name: item.name, url: item.url })
        }>
        <View style={styles.content}>
          <View style={styles.itemView}>
            <Text style={styles.itemText} key={index}>
              #{index + 1} - {item.name}
            </Text>
          </View>
          <View style={styles.icon}>
            <Icon name="angle-right" size={40} color='#2471A3' />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.viewHeader}>
        <Text style={styles.titleHeader}>Pokemon list</Text>
        <Text style={styles.titleResults}>Total results {total}</Text>
      </View>
      <FlatList
        data={pokemones}
        renderItem={renderItem}
        ItemSeparatorComponent={renderSeparator}
      />
      <View style={styles.viewButtons}>
        <View style={styles.viewShowMore}>
          <Button title="Show more" onPress={showMore} color='#808B96' />
        </View>
        <View style={styles.viewRestore}>
          <Button title="Restore" onPress={restore} color='#808B96' />
        </View>
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
    backgroundColor: '#E5E8E8'
  },
  icon: {
    flex: 1,
    alignItems: 'center',
  },
  itemView: {
    flex: 3,
    marginLeft: 20,
  },
  itemText: {
    padding: 5,
    fontSize: 20,
    fontWeight: 'bold',
    color: '#2471A3'
  },
  content: {
    backgroundColor: '#EDF7FA',
    height: 50,
    margin: 7,
    borderRadius: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#0EA1CA',
  },
  viewHeader: {
    margin: 10,
  },
  titleHeader: {
    fontSize: 25,
    color: '#566573',
    fontWeight: 'bold',
  },
  titleResults: {
    fontSize: 15,
    color: '#566573',
  },
  viewButtons: {
    flexDirection: 'row',
  },
  viewShowMore: {
    flex: 1,
    padding: 10,
  },
  viewRestore: {
    flex: 1,
    padding: 10,
  },
});
export default Home;
